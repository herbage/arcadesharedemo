package com.omlet.arcadesharedemo;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                Uri uri = new Uri.Builder()
                        .scheme("omletarcade")
                        // .authority("share")      // share link to Omlet chats or communities
                        .authority("startlive")     // start to live
                        .appendPath("content")
                        .appendQueryParameter("url", "https://arcade.omlet.me") // replace with invitation links here
                        .appendQueryParameter("game", "com.omlet.arcadesharedemo") // replace with the package id of the game
                        .build();
                intent.setData(uri);

                PackageManager pm = getPackageManager();
                boolean installed;
                try {
                    pm.getPackageInfo("mobisocial.arcade", 0);
                    installed = true;
                } catch (PackageManager.NameNotFoundException ignored) {
                    installed = false;
                }

                if (intent.resolveActivity(pm) == null) {
                    if (installed) {
                        Toast.makeText(MainActivity.this, "Please upgrade your Omlet Arcade.", Toast.LENGTH_SHORT).show();
                    }
                    intent.setData(Uri.parse("http://onelink.to/vfwmwk"));
                }

                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(MainActivity.this, "Please install a browser.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
